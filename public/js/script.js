/* The code below it's responsible to set a number of items in the selection field  */
let elements = '';
for (let i = 1; i <= 99; i++) {
    elements += `<option value="${i}">${i}</option>`;
}

document.getElementById("itemQuantity").innerHTML = elements;


// The function below add the fields containing the item name e quantity
const list = [];
const itemName = document.querySelector("#itemName");
const itemQuantity = document.querySelector("#itemQuantity");
const items = document.querySelector(".shopping-items");

function addItemsField() {

    const itemNameValue = itemName.value;
    const itemQuantityValue = itemQuantity.value;

    if(itemName !== "") {
        const item = {
            name: itemNameValue,
            quantity: itemQuantityValue,
        };

        list.push(item);
        renderListItems();
        resetInputs();
    }
}

function renderListItems() {

    let itemsStructure = "";

    list.forEach(function(item) {
        itemsStructure += `
            <li class="shopping-item">
                <span>${item.name}</span>
                <span>${item.quantity}</span>
            </li>
        `;
    });

    items.innerHTML = itemsStructure;
}

function resetInputs() {
    itemName.value = "";
    itemQuantity.value = 1;
}